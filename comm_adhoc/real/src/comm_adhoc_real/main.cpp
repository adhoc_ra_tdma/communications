
#include <string>
#include <future>

#include <signal.h>
#include <sched.h>
#include <unistd.h>


#include <comm_adhoc_implementation/comm_adhoc_implementation.hpp>
#include <rtdbpp/rtdb_comm.hpp>

#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>

std::string getUtilisation(std::string name)
{
	std::stringstream ss;
	std::string tab="    ";
	ss<<"Utilisation:"<<std::endl;
	ss<<tab<<std::string((name.c_str()))<<" "<<" -i <NAME OF THE NETWORK INTERFACE TO USE> -s <PATH TO RTDB CONFIG FILE> [-T <PERIOD (ms) - default=100ms>] [-m <MIN DELAY(ms) - default=0s>] [-M <MAX DELAY(ms) - default=PERIOD>] [-n <ID_NUMBER - USE -1 TO READ FROM ENV(AGENT)>] [-h]";
	return ss.str();
}

std::promise<void> end;



static void signal_catch(int sig)
{
	end.set_value();
}

int main(int argc, char* argv[])
{
	//struct sched_param proc_sched;
	/* Assign a real-time priority to process */
	/*proc_sched.sched_priority=60;
	if ((sched_setscheduler(getpid(), SCHED_FIFO, &proc_sched)) < 0)
	{
		PERRNO("setscheduler");
		return -1;
	}*/


	try
	{

		if(signal(SIGINT, signal_catch) == SIG_ERR)
		{
			PERROR(std::cout,"signal");
			return -1;
		}





		int16_t id=-1;
		std::string ifname="";
		std::string rtdb_config_file="";

		std::chrono::nanoseconds period=std::chrono::milliseconds(100);

		std::chrono::nanoseconds minimum=std::chrono::milliseconds(0);
		std::chrono::nanoseconds maximum=period;
		bool maximum_set=false;

		int opt;
		while ( (opt=getopt(argc, argv, "hn:i:r:m:M:T:")) != -1)
		{
			switch(opt)
			{
				case 'n':
					id=atoi(optarg);
					break;
				case 'm':
					minimum=std::chrono::milliseconds(atoi(optarg));
					break;
				case 'M':
					maximum=std::chrono::milliseconds(atoi(optarg));
					maximum_set=true;
					break;
				case 'T':
					period=std::chrono::milliseconds(atoi(optarg));
					if(!maximum_set) maximum=period;
					break;
				case 'i':
					ifname=std::string(optarg);
					break;
				case 'r':
					rtdb_config_file=std::string(optarg);
					break;
				default:
					std::cout<<"Unknown option "<<opt<<std::endl;
				case 'h':
					std::cout<<getUtilisation(std::string(argv[0]))<<std::endl;
					return 0;
			}
		}


		if( ifname.compare("")==0 )
		{
			std::cout<<getUtilisation(std::string(argv[0]))<<std::endl;
			return -1;
		}


		if( rtdb_config_file.compare("")==0 )
		{
			std::cout<<getUtilisation(std::string(argv[0]))<<std::endl;
			return -1;
		}

		communications::Comm_adhoc_implementation impl(id, ifname, rtdb_config_file, period, minimum, maximum);


		end.get_future().get();


		std::cout<<"Exiting cleanly..."<<std::endl;
		std::cout<<"Goodbye"<<std::endl;
	}
	catch (std::exception& e)
	{
		PERROR(std::cerr , "Exception: " << e.what());
	}

	return 0;
}

