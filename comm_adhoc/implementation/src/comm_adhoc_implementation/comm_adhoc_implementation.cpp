
#include <cstdint>



#include <rtdb_datatypes/count_vector.h>

#include <comm_adhoc_implementation/comm_adhoc_implementation.hpp>


#include <ratdma/synchronisation_robocup2011.hpp>
#include <ratdma/synchronisation_consensus.hpp>

#include <serialisation/serialisationTools.hpp>

#include <debug_headers/clear_debug.hpp>
#ifndef debugLevel
#define debugLevel lError
#endif
#include "debug_headers/debug.hpp"


#define BUFFER_SIZE 2000
#define COMM_DELAY_US 1500

namespace communications
{

Comm_adhoc_implementation::Comm_adhoc_implementation(int16_t id,
								   std::string network_interface,
								   std::string rtdb_configuration,
								   std::chrono::nanoseconds period,
								   std::chrono::nanoseconds minimum,
								   std::chrono::nanoseconds maximum,
								   bool log):
	log(log)
{


	if(log)
	{
		consensus_log=std::unique_ptr<common::Logging::LogStream>(new common::Logging::LogStream("consensus.log"));
		messages_log=std::unique_ptr<common::Logging::LogStream>(new common::Logging::LogStream("message_sequence.log"));
	}


	db=std::unique_ptr<rtdb::rtdb_comm>(new rtdb::rtdb_comm(id, rtdb_configuration));
	if(db->init() == -1)
	{
		PERROR(std::cout,"DB_init");
		throw rtdb_could_not_initialise();
	}
	/*count_vector cnt_vec;
		cnt_vec.value=0;
*/
	if((db->comm_ini(rec)) < 1)
	{
		PERROR(std::cout, "DB_comm_ini");
		throw new rtdb_could_not_retreive_records;
	}

	communications::raTDMA::raTDMA_configuration config;


	// Multicast endpoint
	config.network_device=network_interface;
	config.multicast_address=MULTICAST_ADDRESS;
	config.multicast_port=MULTICAST_PORT;
	// Period and minimum/maximum deviation
	config.period=period;
	config.delta_min=minimum;
	config.delta_max=maximum;
	// State reader and writer
	config.user_send_state=[this](std::vector<uint8_t> &sendBuffer){sender(sendBuffer);};
	config.user_receive_state=[this](const communications::raTDMA::MessageUDP_i &msg){receiver(msg);};

	try
	{

		protocol = std::unique_ptr<communications::raTDMA::raTDMA>(new communications::raTDMA::raTDMA(config));
	}
	catch(const std::exception &e)
	{
		if(protocol.get()!=nullptr)
		{
			protocol.reset();
		}
		PERROR(std::cerr,"Exception: "<<e.what());
		throw e;
	}
}

Comm_adhoc_implementation::~Comm_adhoc_implementation()
{
	protocol.reset();
	db.reset();
}



void Comm_adhoc_implementation::sender(std::vector<uint8_t> &sendBuffer)
{
	int life;
	int myNumber=db->getAgentNumber();

	common::serialisation::Container rtdbData;

	/* TESTES DE CONSENSO */
	/*count_vector cnt_vec;
	if(myNumber!=0)
	{
		db->get(myNumber, COUNTVECTOR, &cnt_vec);
		int mine=cnt_vec.value;
		db->get(0, COUNTVECTOR, &cnt_vec);
		//std::cout<<"AGENT[0] = "<<cnt_vec.value<<" -- mine-prev[myNumber] = "<<mine<<std::endl;
		if(cnt_vec.value!=mine)
		{
			db->put(COUNTVECTOR, &cnt_vec);
		}

//			std::cout<<"AGENT[0] = "<<cnt_vec.value<<std::endl;
//			rtdb->DB_get(1, COUNTVECTOR, &cnt_vec);
//			std::cout<<"AGENT[1] = "<<cnt_vec.value<<std::endl;
//			rtdb->DB_get(2, COUNTVECTOR, &cnt_vec);
//			std::cout<<"AGENT[2] = "<<cnt_vec.value<<std::endl;
	}*/



	// frame header
	_frameHeader frameHeader;
	frameHeader.number = myNumber;
	frameHeader.counter = frameCounter;
	frameCounter ++;




	// prepare message

	for(int agent = 0;agent<N_AGENTS;agent++)
	{
		int sh_recs=db->getSharedRecs(agent);
		for(uint16_t j = 0; j < sh_recs; j++)
		{
			// id
			common::serialisation::serialise(rtdbData,rec[agent].vars[j].id);

			// size
			common::serialisation::serialise(rtdbData,rec[agent].vars[j].size);


			// life and data
			std::vector<uint8_t> data;
			data.resize(rec[agent].vars[j].size);
			life = db->get(agent, rec[agent].vars[j].id, &(data[0]));
			common::serialisation::serialise(rtdbData,life);
			common::serialisation::serialise(rtdbData,data);
			/*for(uint8_t &elem:data)
			{
				rtdb_it=elem;
			}*/
			//std::cout<<"["<<record<<"]"<<"AGENT = "<<agent<<" -- ID = "<<rec[record].id<<" -- SIZE = "<<rec[record].size<<std::endl;
		}
	}
	PINFO(std::cout, "Shared Records#= "<<sharedRecs);
	sendBuffer.insert(sendBuffer.end(), (uint8_t *)&frameHeader, ((uint8_t *)&frameHeader)+sizeof(frameHeader));
	sendBuffer.insert(sendBuffer.end(), rtdbData.begin(), rtdbData.end());

	if (sendBuffer.size() > BUFFER_SIZE)
	{
		PERROR(std::cout, "Frame is bigger that the available buffer.");
		PERROR(std::cout, "Please increase the buffer size or reduce the number of disseminated records");
	}

	//		FDEBUG (filedebug, "config time = %d", (int)((tempTimeStamp.tv_sec - start.tv_sec)*1E6 + tempTimeStamp.tv_usec - start.tv_usec));

	//		FDEBUG(filedebug, "%d\n", (int)((tempTimeStamp.tv_sec - lastSendTimeStamp.tv_sec)*1E6 + tempTimeStamp.tv_usec - lastSendTimeStamp.tv_usec));

	//		FDEBUG (filedebug, "send from %1d->%1d(%4u)-->TTUP=%6d\n", agent[myNumber].inFramePos, myNumber, frameHeader.counter, (int)((tempTimeStamp.tv_sec - lastSendTimeStamp.tv_sec)*1E6 + tempTimeStamp.tv_usec - lastSendTimeStamp.tv_usec));

	/*if(myNumber==0)
	{
		int missing=3;
		db->get(myNumber, COUNTVECTOR, &cnt_vec);
		int mine=cnt_vec.value;
		for(int agent=0;agent<N_AGENTS;agent++)
		{
			db->get(agent, COUNTVECTOR, &cnt_vec);
			if(cnt_vec.value!=mine)
				missing--;
		}
		if(missing==0)
		{
			cnt_vec.value+=1;
			db->put(COUNTVECTOR, &cnt_vec);
		}
	}*/

}


void Comm_adhoc_implementation::receiver(const communications::raTDMA::MessageUDP_i &msg)
{
	std::vector<uint8_t> recvBuffer=msg.getData();
	common::serialisation::Container c;
	int agentNumber;

	PINFO(std::cout,"RECEIVED");

	{
		struct _frameHeader frameHeader;
		int indexBuffer=0;
		memcpy (&frameHeader, &(recvBuffer[0 + indexBuffer]), sizeof(frameHeader));
		indexBuffer += sizeof(frameHeader);


		agentNumber = frameHeader.number;

		PLOG(messages_log, agentNumber<<","<<frameHeader.counter);

		if (agentNumber != db->getAgentNumber())
		{

#warning TODO correction when frameCounter overflows
			//if(lastFrameCounter[agentNumber]!=0)
			{
				if ((lastFrameCounter[agentNumber] + 1) != frameHeader.counter)
				{
					lostPackets[agentNumber] = frameHeader.counter - (lastFrameCounter[agentNumber] + 1);
					total_lostPackets[agentNumber]+=lostPackets[agentNumber];
					PINFO(std::cout, "Lost Packet from agent "<<agentNumber<<". Sequential = "<<lostPackets[agentNumber]<<". Total = "<<total_lostPackets[agentNumber]<<"/"<<frameHeader.counter<<"("<<(100.0*(double)total_lostPackets[agentNumber])/((double)frameHeader.counter)<<"%).");

				}
			}
			lastFrameCounter[agentNumber] = frameHeader.counter;
		}
#warning Idealy this indexBuffer should be removed
		std::vector<uint8_t>::iterator it= recvBuffer.begin();
		std::advance(it,indexBuffer);
		c= common::serialisation::Container(it,recvBuffer.end());
	}


	RTDBconf_var rec;
	// receive from ourself
	// not supposed to occur. just to prevent!
	if (agentNumber != db->getAgentNumber())
	{


		int record=0;
		for(int agent = 0;agent<N_AGENTS;agent++)
		{
			int sh_recs=db->getSharedRecs(agent);
			for(uint16_t j = 0; j < sh_recs; j++, record++)
			{
				// id
				common::serialisation::deserialise(c,rec.id);

				// size
				common::serialisation::deserialise(c,rec.size);

				// life
				int life;
				common::serialisation::deserialise(c,life);
				life += (int)(COMM_DELAY_US/1E3);

				pType data;
				/*for(size_t i=0;i<rec.size;i++)
				{
					#warning Still does not correct endieness problems
					data.push_back(*rtdb_it);
					rtdb_it++;
				}*/
				common::serialisation::deserialise(c,data);


				if(agent!=db->getAgentNumber())
				{
					//std::cout<<"["<<record<<"]"<<"AGENT = "<<agent<<" -- ID = "<<rec.id<<" -- SIZE = "<<rec.size<<std::endl;

					// data
					int size;
					if((size = db->comm_put (agent, rec.id, rec.size, data.data(), life)) != (int)rec.size)
					{
						PERROR(std::cout, "Error in frame/rtdb: from = "<<agentNumber<<", item = "<<rec.id<<", received size = "<<rec.size<<", local size = "<<size);
						break;
					}
				}


			}
		}
	}

	/* TESTES DE CONSENSO */

	/*int myNumber=db->getAgentNumber();
	count_vector cnt_vec;
	if(myNumber==0)
	{
		int age=db->get(myNumber, COUNTVECTOR, &cnt_vec);
		int missing=3;
		int mine=cnt_vec.value;
		for(int agent=0;agent<N_AGENTS;agent++)
		{
			db->get(agent, COUNTVECTOR, &cnt_vec);
			if(cnt_vec.value==mine)
				missing--;
		}
		if(missing==0)
		{
			//std::cout<<"Consensus reached in "<<cycles_to_consensus<<" periods."<<std::endl;
			PLOG(consensus_log, cnt_vec.value <<" "<<age);
		}
	}*/

}



const char *Comm_adhoc_implementation::rtdb_could_not_initialise::what() const noexcept
{
	return "Could not initialise the RTDB";
}

const char *Comm_adhoc_implementation::rtdb_could_not_retreive_records::what() const noexcept
{
	return "Could not retreive the number of records from the rtdb";
}

}
