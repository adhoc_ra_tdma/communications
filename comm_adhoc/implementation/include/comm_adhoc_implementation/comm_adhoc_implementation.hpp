#ifndef __COMM_ADHOC_IMPLEMENTATION_HPP__
#define __COMM_ADHOC_IMPLEMENTATION_HPP__

#include <string>
#include <memory>
#include <exception>

#include "rtdbpp/rtdb_comm.hpp"
#include "rtdb_config/rtdb_user.h"

#include <logging/logging.hpp>
#include <vector>

#include <ratdma/raTDMA_config.hpp>
#include <ratdma/raTDMA.hpp>


#define MULTICAST_ADDRESS "224.16.32.39"
#define MULTICAST_PORT 50000
namespace communications
{


class Comm_adhoc_implementation
{
	public:
		using pType=std::vector<uint8_t>;

		Comm_adhoc_implementation()=delete;

		Comm_adhoc_implementation (int16_t id,
								   std::string network_interface,
								   std::string rtdb_configuration,
								   std::chrono::nanoseconds period,
								   std::chrono::nanoseconds minimum,
								   std::chrono::nanoseconds maximum,
								   bool log=false);

		Comm_adhoc_implementation (const Comm_adhoc_implementation  &other)=delete;
		Comm_adhoc_implementation  &operator=(const Comm_adhoc_implementation  &other)=delete;

		Comm_adhoc_implementation (Comm_adhoc_implementation  &&other)=delete;
		Comm_adhoc_implementation  &operator=(Comm_adhoc_implementation  &&other)=delete;

		virtual ~Comm_adhoc_implementation ();

	private:
		void sender(std::vector<uint8_t> &sendBuffer);
		void receiver(const communications::raTDMA::MessageUDP_i &msg);

	private:
		std::unique_ptr<rtdb::rtdb_comm> db;
		std::unique_ptr<communications::raTDMA::raTDMA> protocol;

		RTDBconf_agent rec[MAX_AGENTS];


		bool log;
		std::unique_ptr<common::Logging::LogStream> consensus_log;
		std::unique_ptr<common::Logging::LogStream> messages_log;


		unsigned int frameCounter = 0;
		unsigned int lastFrameCounter[MAX_AGENTS]={0};
		unsigned int lostPackets[MAX_AGENTS]={0};
		unsigned int total_lostPackets[MAX_AGENTS]={0};

		struct _frameHeader
		{
				uint8_t number;			    // agent number
				uint32_t counter;			// frame counter
		};

		/* Consensus tests */
		int cycles_to_consensus;

	public:

		class rtdb_could_not_initialise: public std::exception
		{
			public:
				rtdb_could_not_initialise() noexcept = default ;
				virtual ~rtdb_could_not_initialise() noexcept = default ;

				virtual const char* what() const noexcept;

		};

		class rtdb_could_not_retreive_records: public std::exception
		{
			public:
				rtdb_could_not_retreive_records() noexcept = default ;
				virtual ~rtdb_could_not_retreive_records() noexcept = default ;

				virtual const char* what() const noexcept;

		};

};



}

#endif
