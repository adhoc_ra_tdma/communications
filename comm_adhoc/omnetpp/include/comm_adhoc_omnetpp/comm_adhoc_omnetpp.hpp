#ifndef __SERVERUDPAPP_HPP__
#define __SERVERUDPAPP_HPP__

#include <applications/common/ApplicationBase.h>
#include <memory>

#include <comm_adhoc_implementation/comm_adhoc_implementation.hpp>
#include <simulatable_timer/simulatable_timer.hpp>

class INET_API comm_adhoc_omnetpp : public cSimpleModule
{
	protected:

	public:
		comm_adhoc_omnetpp();
		~comm_adhoc_omnetpp();

		virtual int numInitStages() const  {return 5;}

	protected:
		virtual void handleMessage(cMessage *msg);

		virtual void initialize(int stage);

		virtual void finish();

		std::unique_ptr<communications::Comm_adhoc_implementation> impl;

		std::unique_ptr<common::simulation_interface::timer::timer_interface> startTimer;
		std::unique_ptr<common::simulation_interface::timer::timer_interface> stopTimer;
		cOutVector eventsFired;
};



#endif /* __MYUDP_APP_HPP__ */
