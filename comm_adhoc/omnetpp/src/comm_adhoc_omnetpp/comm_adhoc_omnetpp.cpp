
#include <comm_adhoc_omnetpp/comm_adhoc_omnetpp.hpp>
#include <simulatable_clock/simulatable_clock.hpp>
#include <string>
#include <vector>
#include <set>
Define_Module(comm_adhoc_omnetpp);


comm_adhoc_omnetpp::comm_adhoc_omnetpp()
{

}

comm_adhoc_omnetpp::~comm_adhoc_omnetpp()
{
}


void setup_start_stop(int id, std::chrono::milliseconds &start, std::chrono::milliseconds &stop)
{

	int event_delta=10000;
	switch(id)
	{
		case 0:
			start=std::chrono::milliseconds(0);
			stop=std::chrono::milliseconds(7*event_delta);
			break;
		case 1:
			start=std::chrono::milliseconds(2*event_delta);
			stop=std::chrono::milliseconds(6*event_delta);
			break;
		case 2:
			start=std::chrono::milliseconds(10);
			stop=std::chrono::milliseconds(4*event_delta);
			break;
		case 3:
			start=std::chrono::milliseconds(event_delta);
			stop=std::chrono::milliseconds(5*event_delta);
			break;
		case 4:
			start=std::chrono::milliseconds(20);
			stop=std::chrono::milliseconds(3*event_delta);
			break;
		default:
			break;
	}

}

void comm_adhoc_omnetpp::initialize(int stage)
{
	cSimpleModule::initialize(stage);

	if (stage == 4)
	{



		std::chrono::milliseconds startTime(par("startTime").longValue());
		std::chrono::milliseconds stopTime(par("stopTime").longValue());
		bool has_stop=(par("stopTime").doubleValue()<0)?false:true;


		std::chrono::milliseconds period(par("period").longValue());
		std::chrono::milliseconds minimum(par("min_delay").longValue());
		std::chrono::milliseconds maximum(par("max_delay").longValue());

		std::string ifname= par("wireless_interface").stdstringValue();
		std::string rtdb_config_file=par("rtdb_config_path").stdstringValue();
		int16_t id=getParentModule()->getParentModule()->getIndex();

		//setup_start_stop(id, startTime, stopTime);
		has_stop=true;

		try
		{


			startTimer=
					common::simulation_interface::timer::new_timer();


			stopTimer=
					common::simulation_interface::timer::new_timer();



			startTimer->start([this,id, ifname, rtdb_config_file, period, minimum, maximum]()
			{

				impl=std::unique_ptr<communications::Comm_adhoc_implementation>(
						 new communications::Comm_adhoc_implementation(id, ifname, rtdb_config_file, period, minimum, maximum)
						 );

			},startTime);


			if(has_stop)
			{
				stopTimer->start([this]()
				{
					try
					{
						impl.reset();
					}
					catch(std::exception &e)
					{
						std::cout<<"!!!!!!!!!!!"<<e.what()<<std::endl;
						throw e;
					}

				},stopTime);
			}
			std::cout<<"Starting simulation at "<<startTime.count()<<"ms and ending at "<<stopTime.count()<<"ms"<<std::endl;

		}
		catch(std::exception &e)
		{
			std::cout<<"Could not lauch app in node "<<id<<" running on iface "<<ifname<<": "<<e.what()<<std::endl;

		}


		eventsFired.setName("Manager events");
	}

}

void comm_adhoc_omnetpp::finish()
{
	// Do something
	impl.reset();
	startTimer.reset();
	stopTimer.reset();
	cSimpleModule::finish();
}

void comm_adhoc_omnetpp::handleMessage(cMessage *msg)
{

}
