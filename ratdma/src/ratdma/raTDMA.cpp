#include <ratdma/raTDMA.hpp>

#include <ratdma/synchronisation_robocup2011.hpp>
#include <ratdma/synchronisation_consensus.hpp>

#include <debug_headers/clear_debug.hpp>

#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lLog
#endif

#include <debug_headers/debug.hpp>

#ifndef __NET_CALC_HPP__
#define __NET_CALC_HPP__


#define DATARATE (11000000.0)
#define BROADCAST_DATARATE (1000000.0)

#define T_PAYLOAD(d,rate) (((d)*8.0)/(rate))
#define T_DIFS 0.000028
#define T_SIFS 0.000010
#define T_BACKOFF (0.000135)
#define T_PREAMBLE 0.000192
#define T_ACK ((T_SIFS)+(T_PREAMBLE)+(T_PAYLOAD(14,1000000)))


#define IP_UDP (28)
#define IEEE80211 (40)
#define T_FRAME(d,rate) (T_DIFS+T_BACKOFF+T_PREAMBLE+T_PAYLOAD(d+IEEE80211,rate))
#define T_UNICAST(d,rate) (T_FRAME(d,rate)+T_ACK)
#define T_BROADCAST(d,rate) (T_FRAME(d,rate))
#define T_MULTICAST(d,rate) (T_BROADCAST(d,rate))

#endif



namespace communications
{
namespace raTDMA
{
raTDMA::raTDMA(raTDMA_configuration config):
	is_configured(false)
  #ifdef WAIT_FOR_ONE
  ,lock(0)
  #endif
{


	try
	{




		this->config=config;


		multicast_device=common::simulation_interface::udp_sockets::new_if_device(config.network_device);
		myAddress=multicast_device->getAddr();
		conMatrix=std::unique_ptr<ConnectivityMatrix_t>(new ConnectivityMatrix_t(myAddress.getBytes()[3], std::chrono::duration_cast<std::chrono::milliseconds>(10*config.period)));

		//sync_tool = std::unique_ptr<communications::raTDMA::SynchronisationI>(new communications::raTDMA::Synchronisation_robocup2011(config.delta_min, config.delta_max));
		sync_tool = std::unique_ptr<communications::raTDMA::SynchronisationI>(new communications::raTDMA::Synchronisation_consensus(myAddress.getBytes()[3], config.delta_min, config.delta_max));


		sendingTimeLog=std::unique_ptr<common::Logging::LogStream>(new common::Logging::LogStream("./sendingTimeLog"+myAddress.getAddr()+".log"));

		sendingTimeCounter=common::simulation_interface::counter::new_counter();


		mcast = common::simulation_interface::udp_sockets::new_udp_multicast_socket(
					config.network_device,udp_endpoint_t(config.multicast_address, config.multicast_port)
					);
		mcast->async_read([this](const MessageUDP_i &msg){this->receiveDone(msg);});

#ifdef WAIT_FOR_ONE
		if(myAddress!=address::from_string("10.0.0.1")) lock.wait();
#endif
		sendTimer=common::simulation_interface::timer::new_timer();
		sendTimer->start([this](){this->fired();},config.period,config.period);
		mcast->start();
		std::cout<< "Started communications: " << myAddress.getAddr() << "="<<
					(uint16_t)myAddress.getBytes()[0]<<"."<<(uint16_t)myAddress.getBytes()[1]<<"."<<(uint16_t)myAddress.getBytes()[2]<<"."<<(uint16_t)myAddress.getBytes()[3]<<"="<<
					myAddress.getInt()<<std::endl<<
					"Period = "<<config.period.count()<<" Min = "<<config.delta_min.count()<<" Max = "<<config.delta_max.count()<<std::endl;
		is_configured=true;
	}
	catch(std::exception &e)
	{
		if(multicast_device.get()!=nullptr)
		{
			multicast_device.reset();
		}

		if(conMatrix.get()!=nullptr)
		{
			conMatrix.reset();
		}

		if(sendingTimeLog.get()!=nullptr)
		{
			sendingTimeLog.reset();
		}

		if(sendingTimeCounter.get()!=nullptr)
		{
			sendingTimeCounter.reset();
		}

		if(mcast.get()!=nullptr)
		{
			mcast.reset();
		}

		if(sendTimer.get()!=nullptr)
		{
			sendTimer.reset();
		}

		is_configured=false;

		PERROR(std::cerr,"Exception: "<<e.what());
		throw e;
	}
}

raTDMA::~raTDMA()
{
	sendTimer.reset();
	mcast.reset();

	multicast_device.reset();
	conMatrix.reset();

	sendingTimeCounter.reset();
	sendingTimeLog.reset();

	sync_tool.reset();

}

void raTDMA::register_trigger_function(std::function<void (clock_t::time_point, clock_t::time_point)> user_trigger)
{
	registered_triggers.push_back(user_trigger);
}

void raTDMA::synchronise(ipv4_t receivedAddress, sim_clock_t::time_point receptionTime)
{
	if(!is_configured) return;
	PDEBUG(std::cout, "Team "<<*conMatrix);

	clock_t::time_point timeToSend(sendTimer->absoluteExpiryTime());

	event_t event;
	event.event_time = receptionTime;
	conMatrix->getLocalID(receivedAddress.getBytes()[3],event.source_id);


	round_t round;
	conMatrix->getLocalID(myAddress.getBytes()[3],round.id);
	round.number_of_slots=conMatrix->getNumberOfMembers();
	round.period = config.period;
	round.slot_start = timeToSend;
	//Not used so far
	std::chrono::nanoseconds round_start_from_epoch=(timeToSend - round.id*(round.period/round.number_of_slots)).time_since_epoch();
	round.phase_offset = std::chrono::nanoseconds(round_start_from_epoch.count()%config.period.count());

	if(conMatrix->get_spanning_connectivity_matrix()(myAddress.getBytes()[3], receivedAddress.getBytes()[3])!=0)
	{

		sync_tool->synchronise(event, round);

	}

}



void raTDMA::fired()
{
	if(!is_configured) return;

	sending.lock();
	try
	{

		round_t round;
		conMatrix->getLocalID(myAddress.getBytes()[3],round.id);
		round.number_of_slots=conMatrix->getNumberOfMembers();
		round.period = config.period;
		clock_t::time_point timeToSend(sendTimer->absoluteExpiryTime());
		round.slot_start = timeToSend-config.period;
		SynchronisationI::duration period_delay=sync_tool->get_round_delay(round);


		sendTimer->delay(period_delay+std::chrono::microseconds( (int64_t)(3000.0*(std::rand()*1.0)/RAND_MAX )));

		sendBuffer.clear();
		//ADD MATRIX DATA
		conMatrix->getPackedState(sendBuffer);
		//ASK DATA FROM USER
		std::vector<uint8_t> data;
		config.user_send_state(data);
		sendBuffer.insert(sendBuffer.end(),data.begin(),data.end());
		//SEND DATA
		MessageUDP_t msg(std::string(config.multicast_address), config.multicast_port,sendBuffer);
		mcast->write(msg);
		period_delay=std::chrono::nanoseconds::zero();
		PLOG(*sendingTimeLog,myAddress.getAddr()<<","<<std::chrono::duration_cast<std::chrono::milliseconds>(sendingTimeCounter->get_time()).count()<<"*");
		sendingTimeCounter->reset();
		//cleanup();
		PINFO(std::cout,"Sent: "<<msg.getData().size());


	}catch (std::exception& e)
	{
		PERROR(std::cerr , "Exception: " << e.what() );
	}
	sending.unlock();
}





void raTDMA::receiveDone(const MessageUDP_i &msg)
{
	if(!is_configured) return;

	std::vector<uint8_t> buffer=msg.getData();
	std::size_t bytes_transferred=buffer.size();
	const udp_endpoint_i &source=msg.getUDPEndpoint();

	if(source.getBytes()[0]==127)
	{
		PLOG(std::cout,"Received from loopback... Ignoring");
		return;
	}

	sim_clock_t::time_point now= sim_clock_t::now();

	double time = T_MULTICAST(bytes_transferred+IP_UDP,BROADCAST_DATARATE);
	sim_clock_t::duration msg_tx_duration = std::chrono::duration_cast<sim_clock_t::duration>(std::chrono::duration<double>(time));

	PLOG(*sendingTimeLog,source.getAddr()<<","<<(std::chrono::duration_cast<std::chrono::milliseconds>(sendingTimeCounter->get_time()-msg_tx_duration).count()));
	sendingTimeCounter->reset(-msg_tx_duration);
	if(source.getInt()!=myAddress.getInt())
	{
		PINFO(std::cout,"Received ("<<bytes_transferred<<") from "<<source.getAddr());
#ifdef WAIT_FOR_ONE
		if(source.address()==address::from_string("10.0.0.1")) lock.post();
#endif
#ifdef SYNCHRONISED
		if(conMatrix->get_state().is_source(source.getBytes()[3]) && conMatrix->get_state().is_sink(source.getBytes()[3]) )
		{
			PDEBUG(std::cout,"Now "<<now.time_since_epoch().count());
			PDEBUG(std::cout,"Message duration "<<msg_tx_duration.count());
			PDEBUG(std::cout,"Diff "<<(now-msg_tx_duration).time_since_epoch().count());
			synchronise(source.getAddr(), now-msg_tx_duration);
		}
#endif
		conMatrix->pushPackedState(source.getBytes()[3], true, buffer);
		MessageUDP_t msg_clean(msg.getUDPEndpoint().getAddr(),msg.getUDPEndpoint().getPort(),buffer);
		config.user_receive_state(msg_clean);
	}
	else
	{
		PLOG(std::cout,"Received from myself");
	}
}
}
}
