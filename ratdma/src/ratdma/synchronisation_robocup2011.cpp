#include <ratdma/synchronisation_robocup2011.hpp>


#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif

#include <debug_headers/debug.hpp>

namespace communications
{

namespace raTDMA
{


Synchronisation_robocup2011::Synchronisation_robocup2011(SynchronisationI::duration delta_min, SynchronisationI::duration delta_max):
	delta_min(delta_min),
	delta_max(delta_max)
{

}

void Synchronisation_robocup2011::synchronise(const Super::event_t &event, const Super::round_t &round)
{

	Super::duration delay=Super::duration::zero();

	uint16_t numberOfSlots=round.number_of_slots;
	uint8_t othersSlot=event.source_id;
	uint8_t mySlot=round.id;

	Super::clock_t::time_point timeToSend=round.slot_start;
	Super::clock_t::time_point prev_timeToSend=timeToSend-round.period;

	// Should run a maximum of 1 time (in normal circumstances)
	while(event.event_time<prev_timeToSend)
	{
		timeToSend=prev_timeToSend;
		prev_timeToSend=timeToSend-round.period;
	}

	int windowsUntilTransmission = ( numberOfSlots - othersSlot + mySlot ) % numberOfSlots;
	PDEBUG(std::cout, "Should send at " << timeToSend.time_since_epoch().count()<<" (" << windowsUntilTransmission<<" windows left)");

	Super::duration timeUntilTransmission = (round.period / numberOfSlots)*windowsUntilTransmission;
	PDEBUG(std::cout, "Time until transmission   " << timeUntilTransmission.count());

	Super::clock_t::time_point newTimeToSend = event.event_time+timeUntilTransmission;
	PDEBUG(std::cout, "Will send at   " << newTimeToSend.time_since_epoch().count());


	if(newTimeToSend>timeToSend)
	{
		delay = newTimeToSend - timeToSend;
		PDEBUG(std::cout, "Was delayed by "<<delay.count());
	}

	duration prev_delay=round_delay.load(std::memory_order_consume);
	duration &max_delay=(prev_delay> delay)?prev_delay:delay;
	round_delay.store(max_delay, std::memory_order_release);

	PDEBUG(std::cout, "Delay was "<<prev_delay.count()<<" now is "<<max_delay.count());

}

SynchronisationI::duration Synchronisation_robocup2011::get_round_delay(const Super::round_t &round)
{

	duration delay=round_delay.exchange(duration::zero(), std::memory_order_acq_rel);

	if(delay<delta_min)
	{
		PDEBUG(std::cout, "Too small");
		delay=std::chrono::nanoseconds::zero();
	}
	else if(delay>delta_max)
	{
		PDEBUG(std::cout, "Too Big: "<<delay.count()<<std::endl<<"Max delay: "<<delta_max.count());
		delay=delta_max;
	}
	if(delay<std::chrono::nanoseconds::zero()) // if there is a delay
	{
		delay=std::chrono::nanoseconds::zero();
	}

	PINFO(std::cout, "Delay "<<delay.count());


	return delay;
}



}

}
