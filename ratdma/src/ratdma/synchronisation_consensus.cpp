#include <ratdma/synchronisation_consensus.hpp>


#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif

#include <debug_headers/debug.hpp>

namespace communications
{

namespace raTDMA
{


double normalise(double angle)
{

	angle=angle-2*M_PI*std::round(angle/(2*M_PI));
	if(angle==M_PI)
		angle=-M_PI;

	return angle;
}

double arc(std::map<uint8_t, clock_t::time_point>)
PHI=s(complete_neighbourhood+1);
PHII=sort(PHI);
PHII2=[PHII(2:end) PHII(1)+2*pi];
DPHII=PHII2-PHII;
DPHII;
C_p=max(DPHII);
C=2*pi-C_p;



Synchronisation_consensus::Synchronisation_consensus(const id_t &id, const duration &delta_min, const duration &delta_max):
	delta_min(delta_min),
	delta_max(delta_max)
{
	round_delay.store(duration::min(), std::memory_order_release);
	state.synchronised=true;
	state.count=HYSTERESIS;
	state.C=0;
}

void Synchronisation_consensus::synchronise(const Super::event_t &event, const Super::round_t &round)
{

	events[event.source_id]=event.event_time;

	//max_consensus(event, round);
	//mean_consensus(event, round);

}

SynchronisationI::duration Synchronisation_consensus::get_round_delay(const Super::round_t &round)
{

	max_consensus(round);
	//mean_consensus(round);

	duration delay=round_delay.exchange(duration::zero(), std::memory_order_acq_rel);


	if(delay==duration::min())
	{
		delay=duration::zero();
	}



	if(delay>delta_max)
	{
		PDEBUG(std::cout, "Too Big: "<<delay.count()<<std::endl<<"Max delay: "<<delta_max.count());
		delay=delta_max;
	}
	else if(delay<delta_min)
	{
		PDEBUG(std::cout, "Too Small: "<<delay.count()<<std::endl<<"Min delay: "<<delta_min.count());
		delay=delta_min;
	}





	PINFO(std::cout," Delay "<<delay.count());
	return delay;
}

void Synchronisation_consensus::max_consensus(const Super::round_t &round)
{

	PDEBUG(std::cout,"-----------------------------------------------------"<<+round.id<<"-----------------------------------------------------");
	PDEBUG(std::cout,"\t-Slot start "<< round.slot_start.time_since_epoch().count());

	for(const std::pair<uint8_t, clock_t::time_point> event:events)
	{
		PDEBUG(std::cout,"--------- Event from "<<+event.first<<" at "<< event.second.time_since_epoch().count() <<"---------");


		Super::clock_t::time_point next_slot=round.slot_start;
		Super::clock_t::time_point prev_slot=next_slot-round.period;


		PDEBUG(std::cout,
			   std::endl<<"Bounds:"<<
			   std::endl<<"\t -Prev slot ="<<prev_slot.time_since_epoch().count()<<
			   std::endl<<"\t -Next slot ="<<next_slot.time_since_epoch().count());

		clock_t::time_point other_ref=event.second-event.first*round.period/round.number_of_slots;
		clock_t::time_point my_ref=prev_slot-round.id*round.period/round.number_of_slots;

		uint64_t k=std::round(other_ref.time_since_epoch().count()/round.period.count());
		uint64_t ref=other_ref.time_since_epoch().count()-k*round.period.count();
		other_ref=clock_t::time_point(clock_t::duration(ref));

		k=std::round(my_ref.time_since_epoch().count()/round.period.count());
		ref=my_ref.time_since_epoch().count()-k*round.period.count();
		my_ref=clock_t::time_point(clock_t::duration(ref));


		duration half_period(round.period/2);

		while(other_ref<my_ref-half_period)
		{
			other_ref+=round.period;
		}
		while(other_ref>=my_ref+half_period)
		{
			other_ref-=round.period;
		}

		clock_t::duration delay=other_ref-my_ref;

		PDEBUG(std::cout,
			   std::endl<<"\t -Other ref = "<<other_ref.time_since_epoch().count()<<
			   std::endl<<"\t -My ref    = "<<my_ref.time_since_epoch().count());



		duration prev_delay=round_delay.load(std::memory_order_acquire);
		duration &max_delay=(prev_delay> delay)?prev_delay:delay;

		round_delay.store(max_delay, std::memory_order_release);

		PDEBUG(std::cout,"Delay = "<<delay.count());




	}
	/*duration prev_delay=round_delay.load(std::memory_order_acquire);

	duration delay_before=prev_delay-round.period;
	duration delay_after=prev_delay+round.period;
	if( std::abs(prev_delay.count()) > std::abs(delay_before.count()))
	{
		std::cout<<"PING"<<std::endl;
		prev_delay=delay_before;
	}
	if( std::abs(prev_delay.count()) > std::abs(delay_after.count()))
	{
		std::cout<<"PONG"<<std::endl;
		prev_delay=delay_after;
	}
	round_delay.store(prev_delay, std::memory_order_release);*/


	events.clear();
	PDEBUG(std::cout, "-----------------------------------------------------"<<+round.id<<"-----------------------------------------------------");

}


void Synchronisation_consensus::mean_consensus(const Super::round_t &round)
{
	PDEBUG(std::cout,"-----------------------------------------------------"<<+round.id<<"-----------------------------------------------------");
	PDEBUG(std::cout,"\t-Slot start "<< round.slot_start.time_since_epoch().count());

	clock_t::duration delay = clock_t::duration::zero();

	for(const std::pair<uint8_t, clock_t::time_point> event:events)
	{
		PDEBUG(std::cout,"--------- Event from "<<+event.first<<" at "<< event.second.time_since_epoch().count() <<"---------");


		Super::clock_t::time_point next_slot=round.slot_start;
		Super::clock_t::time_point prev_slot=next_slot-round.period;

		// Check if event occured before our previous slot
		while(event.second<prev_slot)
		{

			PDEBUG(std::cout,"It was a late event");
			next_slot=prev_slot;
			prev_slot=next_slot-round.period;
		}

		PDEBUG(std::cout,
			   std::endl<<"Bounds:"<<
			   std::endl<<"\t -Prev slot ="<<prev_slot.time_since_epoch().count()<<
			   std::endl<<"\t -Next slot ="<<next_slot.time_since_epoch().count());

		clock_t::time_point other_ref=event.second-event.first*round.period/round.number_of_slots;
		clock_t::time_point my_ref=prev_slot-round.id*round.period/round.number_of_slots;

		duration half_period(round.period/2);

		if(other_ref<my_ref-half_period)
		{
			other_ref+=round.period;
		}
		else if(other_ref>=my_ref+half_period)
		{
			other_ref-=round.period;
		}

		delay+=other_ref-my_ref;

		PDEBUG(std::cout,
			   std::endl<<"\t -Other ref = "<<other_ref.time_since_epoch().count()<<
			   std::endl<<"\t -My ref    = "<<my_ref.time_since_epoch().count());





		PDEBUG(std::cout,"Delay = "<<delay.count());

		round_delay.store(duration((int64_t)(0.9*delay.count())), std::memory_order_release);



	}
	/*	duration prev_delay=round_delay.load(std::memory_order_acquire);

		duration delay_before=prev_delay-round.period;
		duration delay_after=prev_delay+round.period;
		if( std::abs(prev_delay.count()) > std::abs(delay_before.count()))
		{
			std::cout<<"PING"<<std::endl;
			prev_delay=delay_before;
		}
		if( std::abs(prev_delay.count()) > std::abs(delay_after.count()))
		{
			std::cout<<"PONG"<<std::endl;
			prev_delay=delay_after;
		}
		round_delay.store(prev_delay, std::memory_order_release);*/


	events.clear();
	PDEBUG(std::cout, "-----------------------------------------------------"<<+round.id<<"-----------------------------------------------------");


}



}

}
