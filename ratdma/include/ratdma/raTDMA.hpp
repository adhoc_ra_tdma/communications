#ifndef __RATDMA_HPP__
#define __RATDMA_HPP__

#define SYNCHRONISED
//#define WAIT_FOR_ONE

#include <ratdma/raTDMA_config.hpp>
#include <logging/logging.hpp>
#include <ratdma/synchronisation_interface.hpp>

namespace communications
{
namespace raTDMA
{

class raTDMA
{
    public:
		raTDMA(raTDMA_configuration config);


		raTDMA()=delete;

		raTDMA(const raTDMA &other)=delete;
		raTDMA &operator=(const raTDMA &other)=delete;

		raTDMA(raTDMA &&other)=delete;
		raTDMA &operator=(raTDMA &&other)=delete;

		virtual ~raTDMA();


		void register_trigger_function(std::function<void(clock_t::time_point start_time,clock_t::time_point stop_time)> user_trigger);


    protected:


		void synchronise(ipv4_t receivedAddress, sim_clock_t::time_point receptionTime);
        void fired();


        void sendDone(std::size_t bytes_transferred);
		void receiveDone(const MessageUDP_i &msg);

    private:

		/* Slot triggers */

		std::list<std::function<void(clock_t::time_point start_time,clock_t::time_point stop_time)>> registered_triggers;

        /* raTDMA config */
		raTDMA_configuration config;
        /* */

		std::unique_ptr<network_device_i> multicast_device;
		ipv4_t myAddress;

		bool is_configured;

#ifdef WAIT_FOR_ONE
        boost::interprocess::interprocess_semaphore lock;
#endif

		std::mutex sending;
		std::unique_ptr<ConnectivityMatrix_t> conMatrix;


		std::unique_ptr<counter_i> sendingTimeCounter;
		std::unique_ptr<common::Logging::LogStream> sendingTimeLog;

		/* Socket and Timer Services */
		std::vector<uint8_t> receiveBuffer;
		std::vector<uint8_t> sendBuffer;

		std::unique_ptr<timer_i> sendTimer;
		std::unique_ptr<udp_multicast_socket_t> mcast;

		/* Synch tool */
		std::unique_ptr<communications::raTDMA::SynchronisationI> sync_tool;
};
}
}

#endif
