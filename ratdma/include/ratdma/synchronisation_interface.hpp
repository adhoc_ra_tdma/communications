#ifndef __SYNCHRONISATION_INTERFACE_HPP__
#define __SYNCHRONISATION_INTERFACE_HPP__


#include <ratdma/raTDMA_config.hpp>

namespace communications
{
namespace raTDMA
{

class SynchronisationI
{

	public:
		using clock_t=raTDMA::clock_t;
		using duration=clock_t::duration;

		using event_t=raTDMA::event_t;
		using round_t=raTDMA::round_t;

		virtual ~SynchronisationI()=default;

		virtual void synchronise(const event_t &event, const round_t &round)=0;
		virtual duration get_round_delay(const round_t &round)=0;



};

}
}


#endif
