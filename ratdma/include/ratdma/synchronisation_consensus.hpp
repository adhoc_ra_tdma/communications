#ifndef __SYNCHRONISATION_CONSENSUS_HPP__
#define __SYNCHRONISATION_CONSENSUS_HPP__

#include <ratdma/synchronisation_interface.hpp>
#include <ratdma/raTDMA_config.hpp>

#include <map>

namespace communications
{
namespace raTDMA
{



class Synchronisation_consensus: public SynchronisationI
{

	private:
		using Super=SynchronisationI;

	public:

		Synchronisation_consensus ()=delete;

		Synchronisation_consensus (const id_t &id, const duration &delta_min, const duration &delta_max);

		Synchronisation_consensus (const Synchronisation_consensus  &other)=delete;
		Synchronisation_consensus  &operator=(const Synchronisation_consensus  &other)=delete;

		Synchronisation_consensus (Synchronisation_consensus  &&other)=delete;
		Synchronisation_consensus  &operator=(Synchronisation_consensus  &&other)=delete;

		virtual ~Synchronisation_consensus()=default;

		virtual void synchronise(const event_t &event, const round_t &round) override;
		virtual duration get_round_delay(const Super::round_t &round) override;

	private:

		const static std::size_t HYSTERESIS=5;

		struct synch_state
		{
				bool synchronised;
				std::size_t count;
				std::size_t C;
		} state;

		void max_consensus(const round_t &round);
		void mean_consensus(const round_t &round);


		std::map<uint8_t, clock_t::time_point> events;

		std::atomic<duration> round_delay;

		duration delta_min;
		duration delta_max;



};


}
}


#endif
