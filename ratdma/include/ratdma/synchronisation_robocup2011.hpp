#ifndef __SYNCHRONISATION_ROBOCUP2011_HPP__
#define __SYNCHRONISATION_ROBOCUP2011_HPP__

#include <ratdma/synchronisation_interface.hpp>
#include <ratdma/raTDMA_config.hpp>

namespace communications
{
namespace raTDMA
{



class Synchronisation_robocup2011: public SynchronisationI
{

	private:
		using Super=SynchronisationI;

	public:

		Synchronisation_robocup2011 (duration delta_min, duration delta_max);

		Synchronisation_robocup2011()=delete;

		Synchronisation_robocup2011 (const Synchronisation_robocup2011  &other)=delete;
		Synchronisation_robocup2011  &operator=(const Synchronisation_robocup2011  &other)=delete;

		Synchronisation_robocup2011 (Synchronisation_robocup2011  &&other)=delete;
		Synchronisation_robocup2011  &operator=(Synchronisation_robocup2011  &&other)=delete;

		virtual ~Synchronisation_robocup2011()=default;

		virtual void synchronise(const event_t &event, const round_t &round) override;

		virtual duration get_round_delay(const Super::round_t &round) override;

	private:

		std::atomic<duration> round_delay;

		duration delta_min;
		duration delta_max;

};


}
}


#endif
