#ifndef __RATDMA_CONFIG_HPP__
#define __RATDMA_CONFIG_HPP__




#include <chrono>
#include <functional>

#include <simulatable_clock/simulatable_clock.hpp>
#include <simulatable_timer/simulatable_timer.hpp>
#include <simulatable_sockets/simulatable_sockets.hpp>

#include <network_generics/udp_message.hpp>
#include <relative_localisation/relative_localisation.hpp>



namespace communications
{
namespace raTDMA
{

using id_t=uint8_t;

using clock_t=common::simulation_interface::simulation_clock;

using timer_i=common::simulation_interface::timer::timer_interface;
using counter_i=common::simulation_interface::counter::counter_interface;
using sim_clock_t=common::simulation_interface::simulation_clock;

using network_device_i=common::simulation_interface::udp_sockets::network_device_interface;
using udp_multicast_socket_t=common::simulation_interface::udp_sockets::multicast_socket_interface;
using udp_endpoint_i=common::simulation_interface::udp_sockets::udp_endpoint_interface;
using udp_endpoint_t=common::network::generics::udp_endpoint;
using MessageUDP_t=common::network::generics::MessageUDP;
using MessageUDP_i=common::network::generics::MessageUDPI;
using ipv4_t=common::network::generics::ipv4;

using ConnectivityMatrix_t=common::relative_localisation::ConnectivityMatrix<uint8_t,id_t, id_t >;

struct raTDMA_configuration
{
		// Network device where we listen for the multicast packets
		std::string network_device;

		// Multicast endpoint
		std::string multicast_address;
		std::uint16_t multicast_port;

		// Period and minimum/maximum deviation
		std::chrono::nanoseconds period;
		std::chrono::nanoseconds delta_min;
		std::chrono::nanoseconds delta_max;

		// State reader and writer
		std::function<void(std::vector<uint8_t> &)> user_send_state;
		std::function<void(const MessageUDP_i &)> user_receive_state;

};

struct round_t
{
		std::chrono::nanoseconds phase_offset;
		clock_t::time_point slot_start;
		std::uint8_t id;
		std::chrono::nanoseconds period;
		std::uint16_t number_of_slots;
};

struct event_t
{
		std::uint8_t source_id;
		clock_t::time_point event_time;
};

}
}

#endif
